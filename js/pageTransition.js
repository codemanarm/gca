const headerNavs = [...document.querySelectorAll('.header_nav_item')];
const headerLogo = document.querySelector('.header_logo');
let activeHeader;

const options = [
    {
      from: '(.*)',
      to: 'custom-transition',
        in: function(next) {
            var tl = gsap.timeline({duration: 0.5});
            tl.fromTo('.animation_item_1', {y: "0%",}, {y: "100%", ease: "power1.in", duration: 0.3});
            tl.fromTo('.animation_item_2', {y: "0%",}, {y: "100%", ease: "power1.in", duration: 0.3});
            tl.fromTo('.animation_item_3', {y: "0%",}, {y: "100%", ease: "power1.in", duration: 0.3});
            tl.fromTo('.animation_item_4', {y: "0%",}, {y: "100%", ease: "power1.in", duration: 0.3});
            tl.fromTo('.animation_item_5', {y: "0%",}, {y: "100%", ease: "power1.in", duration: 0.3});
            tl.to('.animation_container', {zIndex: -1}, ">");
            tl.to('.animation_item', {display: 'none'},">");
            tl.eventCallback("onComplete", next)
      },

      out: (next) => {
        var tl = gsap.timeline({duration: 0.5});
        tl.set('.animation_container', {zIndex: 5});
        tl.set('.animation_item', {y: "100%", display: 'block'});
        tl.fromTo('.animation_item_1', {y: "100%",duration: 0.3}, {y: "0", ease: "power3.out",duration: 0.3});
        tl.fromTo('.animation_item_2', {y: "100%",duration: 0.3}, {y: "0", ease: "power3.out",duration: 0.3});
        tl.fromTo('.animation_item_3', {y: "100%",duration: 0.3}, {y: "0", ease: "power3.out",duration: 0.3});
        tl.fromTo('.animation_item_4', {y: "100%",duration: 0.3}, {y: "0", ease: "power3.out",duration: 0.3});
        tl.fromTo('.animation_item_5', {y: "100%",duration: 0.3}, {y: "0", ease: "power3.out",duration: 0.3});
        tl.eventCallback("onComplete", next)
      }
    },
     {
    from: '(.*)',
    to: '(.*)',
    in: function(next) {
      document.querySelector('#swup').style.opacity = 0;
      TweenLite.to(document.querySelector('#swup'), 0.5, {
        opacity: 1,
        onComplete: next
      });
    },
    out: (next) => {
      document.querySelector('#swup').style.opacity = 1;
      TweenLite.to(document.querySelector('#swup'), 0.5, {
        opacity: 0,
        onComplete: next
      });
    }
  }
  ];

const changeRouteNameForHistoryAPI = (homepageRoute, navigationElements) =>{
    const getRouteName = location.pathname.replace('/','').replace('.html','');

    if(getRouteName == homepageRoute) return;

    let getCurrentNavigationItem = navigationElements.find(item => {
        return item.dataset.navigation === getRouteName
    });

    getCurrentNavigationItem && getCurrentNavigationItem.classList.add('header_nav_item-active')
    activeHeader = getCurrentNavigationItem
}


headerLogo.addEventListener('click',() => {
    activeHeader && activeHeader.classList.remove('header_nav_item-active');
})

headerNavs.forEach(item => {
    item.addEventListener('click', (e) => {
        if(activeHeader) activeHeader.classList.remove('header_nav_item-active');

        e.target.classList.add('header_nav_item-active');
        activeHeader = e.target;
    })
})


document.addEventListener('swup:contentReplaced', () => {
    changeRouteNameForHistoryAPI('home', headerNavs)
});

window.addEventListener('load', () => {
    changeRouteNameForHistoryAPI('home', headerNavs);
    if(document.querySelector('.sponsors_page') !== null){
        initFormPopupEvents('gca--open_form', '#gca--form-submissionSponsor');
        initFormPopupEvents('gca--open_form', '#gca--form-submissionPartner');
    }
})


var swiper;
function popupToggler(element,overlay,closeBtn){
    if(closeBtn){
        element.classList.remove('collapsed');
        overlay.classList.remove('show');
        document.body.classList.remove('collapsed');
        return;
    }
    
    element.classList.add('collapsed');
    overlay.classList.add('show');
    document.body.classList.add('collapsed');
};

function initFormPopupEvents(formButtonClassName, formContainer){
    const sponsorForm = document.querySelector(formContainer);
    
    const formBtn = document.querySelector(`.${formButtonClassName}`);
    const overlay = document.querySelector('.overlay');
    const overlayCloseBtn = document.querySelector('.close_button');

    formBtn.addEventListener('click', () => {
        popupToggler(sponsorForm, overlay)
    });

    overlayCloseBtn.addEventListener('click', () => {
        const currentElem = document.querySelector('.gca--form-submission-wrapper.collapsed');
        popupToggler(currentElem, overlay, true)
    });
}
function init() {
    if (document.querySelector('.speakers_page') !== null) {
        initData();
    }

    if(document.querySelector('body.home') !== null){
        const betBtn = document.querySelector('.hero_arrow');

        swiper = new Swiper('.swiper-container', {
            spaceBetween: 50,
            centeredSlides: true,
            slidesPerView: 1,
            loop: true,
            autoplay: {
              delay: 4500,
              disableOnInteraction: false,
            },
        });
        
        betBtn.addEventListener('click', () => {
            document.querySelector('.about').scrollIntoView({
                behavior: "smooth",
                block: "center",
            })
        })
    }

    if(document.querySelector('.schedule_page') !== null){
        initSchedule()
    }

    if(document.querySelector('.speakers_page')){
        textReveal();
    }

    if(document.querySelector('.sponsors_page') !== null){
        initFormPopupEvents('gca--open_form', '#gca--form-submissionSponsor');
        initFormPopupEvents('gca--open_form', '#gca--form-submissionPartner');
    }
}

function textReveal() {
    const hostAnimationTitleElem = document.querySelector('.text-reveal');
    const getContent = hostAnimationTitleElem.textContent;
    hostAnimationTitleElem.innerHTML=''
    const getChars = getContent.split('');
    console.log(getChars);
    
    for (let i = 0; i < getChars.length; i++) {
        hostAnimationTitleElem.innerHTML += `<span>${getChars[i]}</span>`
    }
    let char = 0;
    let timer = setInterval(onTick, 50);

    function onTick() {
        const span = hostAnimationTitleElem.querySelectorAll('span')[char];
        span.classList.add('text-fade');
        char++;
        
        if (char == getChars.length) {
            clearInterval(timer);
            timer= null;
            return;
        }
    }

}

function unload() {
    if (document.querySelector('body.home')) {
        swiper && swiper.destroy()
    }
}

  const swup = new Swup({
      plugins: [
        new SwupBodyClassPlugin(),
        new SwupJsPlugin(options)
      ]
  });

  init();

  swup.on('contentReplaced', init);
  swup.on('willReplaceContent', unload);


  
const bodyClassPlugin = new SwupBodyClassPlugin({
    prefix: ''
});

