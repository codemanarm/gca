var container;
var camera, scene, renderer;
var uniforms;
var mesh, geometry, light;
let time = 0;

container = document.getElementById("home_container");

var width = container.clientWidth;
var height = container.clientHeight;

function initAnim() {
  container = document.getElementById("home_container");

  camera = new THREE.PerspectiveCamera(75, width / height, 1, 1000);

  scene = new THREE.Scene();

  uniforms = {
    u_time: {
      type: "f",
      value: 1.0
    },
    u_resolution: {
      type: "v2",
      value: new THREE.Vector2()
    },
    u_mouse: {
      type: "v2",
      value: new THREE.Vector2()
    },
    bg: {
      type: "f",
      value: new THREE.TextureLoader().load("./assets/images/bg.jpg")
    }
  };

  var geometry = new THREE.BoxGeometry(1, 1, 1);

  var material = new THREE.ShaderMaterial({
    uniforms: uniforms,
    vertexShader: document.getElementById("vertexShader").textContent,
    fragmentShader: document.getElementById("fragmentShader").textContent,
    wireframe: false,
    side: THREE.DoubleSide
  });

  light = new THREE.SpotLight(0x404040);
  light.castShadow = true;
  light.position.y = 80;
  light.position.z = -10;
  light.intensity = 1;

  scene.add(light);

  material = new THREE.MeshStandardMaterial({ color: 0x0d2348 });

  mesh = new THREE.Mesh(geometry, material);

  mesh.castShadow = true; //default is false
  mesh.receiveShadow = false;

  scene.add(mesh);
  mesh.name = "Box";

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);

  container.appendChild(renderer.domElement);

  onWindowResize();
  window.addEventListener("resize", onWindowResize, false);

  document.onmousemove = function(e) {
    uniforms.u_mouse.value.x = e.pageX;
    uniforms.u_mouse.value.y = e.pageY;
  };

  box = scene.getObjectByName("Box");
  box.scale.multiplyScalar(1.5);
  scene.remove(box);

  camera.position.y = 60;
  camera.position.z = 100;

  group = new THREE.Group();
  group.position.y = 62;
  group.position.x = -50;

  //   group.position.z = 0;

  group.rotation.x = -50;
  scene.add(group);
  for (var x = 0; x <= 50; x++) {
    for (var z = 0; z <= 100; z++) {
      var clone = box.clone();
      clone.material = box.material.clone();
      clone.position.x = x * 2;
      clone.position.z = z * 2;
      group.add(clone);
    }
  }
}

function onWindowResize(event) {
  width = container.clientWidth;
  height = container.clientHeight;
  renderer.setSize(width, height);
  uniforms.u_resolution.value.x = width;
  uniforms.u_resolution.value.y = height;
  camera.updateProjectionMatrix();
}

function animate() {
  
    requestAnimationFrame(animate);
  
    time += 0.0080;
    camera.position.x = Math.sin(time * 0.3) * 50;
  
    //   light.position.z = Math.sin(time * 0.3) * 50;
    //camera.position.y = Math.cos(time * 0.3) * 20;
    camera.lookAt(new THREE.Vector3(0, -2, 0));
    var center = new THREE.Vector3(50, 0, 50);
    var children = group.children;
    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      child.position.y =
        Math.sin(time * 1.12 + child.position.x * 0.3) +
        Math.sin(time * 2.23 + child.position.z * 0.3);
      child.position.y *=
        Math.pow(1 - Math.min(20, child.position.distanceTo(center)) / 10, 2) * 2;
    }
  
    render();

}

function render() {
  uniforms.u_time.value += 0.05;
  renderer.render(scene, camera);
}
