module.exports =  [
    {
        id: `${Math.floor(Math.random * Math.floor(1000))}`,
        name: 'Andrey Popovich',
        title: 'Indie game development',
        separatedTitle: 'Making hobby into successful business',
        profession: 'Self-employed Lead Game Designer',
        workplace: 'Former Senior Game Designer at Wargaming',
        imgageId: 1
    },
    {
        id: `${Math.floor(Math.random * Math.floor(1000))}`,
        name: 'Elina Asriyan',
        title: 'The Psychology of Video Games:',
        separatedTitle: 'Common myths and stereotypes',
        profession: 'PhD in Psychology',
        workplace: 'Yerevan State University Vice-Rector',
        imgageId: 2
    },
    {
        id: `${Math.floor(Math.random * Math.floor(1000))}`,
        name: 'Rouben Meschian',
        host: true,
        title: 'The Psychology of Video Games:',
        profession: 'Co-Founder of JS Conf Armenia',
        workplace: 'Founding Engineer at Cambridge Semantics',
        imgageId: 3
    }
]